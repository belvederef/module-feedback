<!DOCTYPE html>
<head>
  <title>Student Survey</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="js/jquery.js"></script>
  <link rel=stylesheet href=stylesheet.css>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<?php
print "<h1 class='text-center'>Module feedback</h1>";

if (!array_key_exists('u', $_REQUEST)){
  print "<div class=student_id>Who are you? <form><input name=u value='50200036'><input type='submit'></form></div>";
  exit();
}

//Connect to database. If fails, give error messeges
$con = new mysqli('localhost','40218080', 'jFiikT32', '40218080');
if ($con->connect_errno){
  die("Failed to connect to MySQL: (" . $con->connect_errno . ") "
      . $con->connect_error);
}

///Get the student name
if (!($stmt = $con->prepare(
"SELECT SPR_FNM1
      , SPR_SURN
      , MOD_CODE 
  FROM INS_SPR JOIN CAM_SMO ON (CAM_SMO.SPR_CODE=INS_SPR.SPR_CODE)
WHERE INS_SPR.SPR_CODE=?")))
  die("Prepare failed: (" . $con->errno . ") " . $con->error); 

//Bind parameters to query
if (!$stmt->bind_param("s", $_REQUEST['u']))
  die("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);

if (!$stmt->execute())
  die("Execute failed: (" . $stmt->errno . ") " . $stmt->error);

//Fetch results into cur to get the student's name
$cur = $stmt->get_result();
if(!($row = $cur->fetch_array())){
  echo "<h5 class='text-center'>Matric number is not valid!</h5>";
  exit();
}
print "<h5 class='text-center'>Welcome student: ".$row[0].' '.$row[1]."</h5>";

$stmt->close();



///Get the student's modules
//Prepare statement 1
if (!($stmt = $con->prepare(
"SELECT INS_MOD.MOD_CODE
      , MOD_NAME
  FROM CAM_SMO JOIN INS_MOD ON (CAM_SMO.MOD_CODE=INS_MOD.MOD_CODE)
               LEFT JOIN INS_PRS ON (INS_MOD.PRS_CODE=INS_PRS.PRS_CODE)
WHERE SPR_CODE=? AND AYR_CODE='2016/7' AND PSL_CODE='TR1';")))
  die("Prepare 1 failed: (" . $con->errno . ") " . $con->error);

//Bind parameters to query 1
if (!$stmt->bind_param("s", $_REQUEST['u']))
  die("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);

if (!$stmt->execute())
  die("Execute stmt failed: (" . $stmt->errno . ") " . $stmt->error);

//Fetch results for stmt1 into cur
$cur = $stmt->get_result();
$stmt->close();


///Prepare statement to get the questions to be asked
if (!($stmt = $con->prepare(
"SELECT INS_QUE.QUE_CODE
      , QUE_TEXT
 FROM INS_QUE;")))
  die("Prepare 2 failed: (" . $con->errno . ") " . $con->error);

if (!$stmt->execute())
  die("Execute stmt2 failed: (" . $stmt->errno . ") " . $stmt->error);

//Fetch results stmt2 into cur2
$cur2 = $stmt->get_result();
//Store all the questions in an array to be looped though several times 
//as the questions are the same for every module
$res = $cur2->fetch_all();



///Get the answers back and use to highlight radio buttons
if (!($stmt_chk = $con->prepare(
"SELECT QUE_CODE
      , RES_VALU
 FROM INS_RES
 WHERE SPR_CODE=? AND MOD_CODE=? 
    AND AYR_CODE='2016/7' AND PSL_CODE='TR1';")))
  die("Prepare 2 failed: (" . $con->errno . ") " . $con->error);



print "<div class='centered'>";
  print "<form action=store_res.php>";
    print "<table id='modules'>";

    //Get the student's module
    while($row = $cur->fetch_row()){
      print "<tr class='toggler'><td>$row[0]</td><td>$row[1]</td></tr>";
      print "<tr><td colspan=2>";
        print "<div class='module'>";
          print "<h3>Answer the following questions for $row[0]</h3>";

          //Bind parameters so to get answers only for the specified module
          if (!$stmt_chk->bind_param("ss", $_REQUEST['u'], $row[0]))
            die("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);

          //Get the questions for each module
          foreach($res as $row2){
            print "<p>$row2[0] $row2[1]</p>";
            print "<div class='radio_btn'>";

            //Create radio buttons and "check" appropriate ones
            for($i=1; $i<6;$i++){   
              if (!$stmt_chk->execute())
                die("Execute stmt_chk failed: (" . $stmt->errno . ") " . $stmt->error);

              $cur_chk = $stmt_chk->get_result();
              $is_checked='';

              while ($row_chk = $cur_chk->fetch_row()){      
                if($row_chk[0] == $row2[0] && $row_chk[1] == $i) {
                  $is_checked = 'checked';
                }
              }
              print "<input type='radio' name='$row[0]_Q$row2[0]' value='$i' $is_checked>";
                switch($i) {
                  case 1: print "DA"; break;
                  case 2: print "MA"; break;
                  case 3: print "N"; break;
                  case 4: print "MD"; break;
                  case 5: print "DD"; break;
              }
            }
            print "</div>";
            print "<hr>";
          }
        print "</div>";
      print "</td></tr>";
    }
    print "</table>";
  print "<input type='hidden' name=u value=$_REQUEST[u]>";
  print "<input id='submit' type='submit' value='Submit'>";
  print "</form>";
print "</div>";


