# README #

### What is this repository for? ###

All modules running at Edinburgh Napier University are reviewed using a standard set of questions answered by students studying the modules.
The current system is paper-based so I built an online system to allow students to provide feedback using web pages.

The input screen is for students to enter their feedback for modules. This screen is responsive.
The output screen is for teaching staff to view the feedback. It allows staff to enter a single module number for the output scores screen.
It also allows you to enter two module numbers for the output comparison screen with relative graphs. 

It can be temporarily accessed at http://40218080.set08101.napier.ac.uk/cw2/
Also, http://40218080.set08101.napier.ac.uk/cw2/output.html contains the module comparison page.