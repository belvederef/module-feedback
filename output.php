<?php
//Redirect to the main page is a module code is not chosen
if(!array_key_exists('MOD_CODE', $_REQUEST)){
  header('Location: '.'output.html');
  exit();
}


//Connect to database. If fails, give error messeges
$con = new mysqli('localhost','40218080', 'jFiikT32', '40218080');
if ($con->connect_errno){
  die("Failed to connect to MySQL: (" . $con->connect_errno . ") "
      . $con->connect_error);
}


///Get the student name
//Prepare statement
if (!($stmt = $con->prepare(
"SELECT INS_RES.MOD_CODE
      , CAT_SNAM
      , INS_RES.QUE_CODE
      , QUE_NAME
      , ROUND(100*AVG(CASE WHEN RES_VALU IN (4,5) THEN 1 ELSE 0 END)) AS v
      , COUNT(RES_VALU)
  FROM INS_RES JOIN INS_QUE ON (INS_RES.QUE_CODE=INS_QUE.QUE_CODE)
               JOIN INS_CAT ON (INS_QUE.CAT_CODE=INS_CAT.CAT_CODE)
WHERE MOD_CODE=?
GROUP BY INS_RES.MOD_CODE, CAT_SNAM, INS_RES.QUE_CODE, INS_QUE.QUE_NAME
ORDER BY INS_RES.QUE_CODE")))
  die("Prepare failed: (" . $con->errno . ") " . $con->error);

//Bind parameters to query
if (!$stmt->bind_param("s", $_REQUEST['MOD_CODE']))
  die("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);

//Execute stmt
if (!$stmt->execute())
  die("Execute stmt failed: (" . $stmt->errno . ") " . $stmt->error);

//Fetch results for stmt1
$res = $stmt->get_result();



//Bind parameters to query 2
if (!$stmt->bind_param("s", $_REQUEST['MOD_CODE2']))
  die("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);

//Execute stmt 2
if (!$stmt->execute())
  die("Execute stmt failed: (" . $stmt->errno . ") " . $stmt->error);

//Fetch results for stmt2
$res2 = $stmt->get_result();

print json_encode(array("a" => $res->fetch_all(), "b" => $res2->fetch_all()));
$stmt->close();

