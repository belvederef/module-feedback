<!DOCTYPE html>
<head>
  <title>Completed | Student Survey</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="js/jquery.js"></script>
  <link rel=stylesheet href=stylesheet.css>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<?php
print "<h1 class='text-center'>Module feedback</h1>";

if (!array_key_exists('u', $_REQUEST)){
  die("<h5 class='text-center'>If you see this page, there has been an error.
      Please go back to the <a href='index.php'>main page.</a></h5>");
}

//Connect to database. If fails, give error messeges
$con = new mysqli('localhost','40218080', 'jFiikT32', '40218080');
if ($con->connect_errno){
  die("Failed to connect to MySQL: (" . $con->connect_errno . ") "
      . $con->connect_error);
}

//Delete previously inserted responses
if (!($stmt = $con->prepare(
  "DELETE FROM INS_RES 
   WHERE SPR_CODE=? AND AYR_CODE='2016/7' AND PSL_CODE='TR1'")))
  die("Questions deletion failed: (" . $con->errno . ") " . $con->error);

if (!$stmt->bind_param("s", $_REQUEST['u']))
  die("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);

if (!$stmt->execute())
  die("Execute failed: (" . $stmt->errno . ") " . $stmt->error);



//Add new responses
if (!($stmt = $con->prepare(
  "INSERT INTO INS_RES
   VALUES (?,?,'2016/7','TR1',?,?)")))
  die("Prepare failed: (" . $con->errno . ") " . $con->error); 

if (!$stmt->bind_param("sssi", $_REQUEST['u'], $mc, $qn, $res))
  die("Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error);

//Build data to insert in the sql statement
foreach($_REQUEST as $k => $v){
  if ($k == 'u')
    continue;
  
  $mc_qn = preg_split('/_Q/', $k);
  $mc = $mc_qn[0];
  $qn = $mc_qn[1] = preg_replace('/_/', '.', $mc_qn[1]);

  $res = $v;

  if (!$stmt->execute())
    die("Execute failed: (" . $stmt->errno . ") " . $stmt->error);
}

print "<div class='centered'>";
  print "<h3 class='text-center'>Thank you for completing this survey</h3>";
  print "<h4 class='text-center'>You may close this page now</h4>";
print "</div>";
